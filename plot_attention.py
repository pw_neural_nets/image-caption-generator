from model import experiment

exp = experiment.ExperimentAttention.from_checkpoint()

phase = 'train'
for X, y in exp.databunch[phase]:
    for i in range(0, X[1].shape[0], 5):
        img = X[1][i]
        save_path = f"./attention_examples/{phase}/att_img_{i}"
        exp.plot_attention(img.unsqueeze(0), save_path=save_path)
    break
