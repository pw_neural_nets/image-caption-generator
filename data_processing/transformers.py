import torch
from torch.nn.utils.rnn import pad_sequence

#TODO
# add image preprocessors
# - cropout


def text_to_model_input(text):
    return text[..., :-1]


def batch_text_padding(batch):
    idxs, imgs, texts = zip(*batch)
    texts = [torch.LongTensor(tok) for tok in texts]
    texts_padded = pad_sequence(texts, batch_first=True, padding_value=1)
    imgs = torch.stack(imgs)
    idxs = torch.LongTensor(idxs)
    return (idxs, imgs, text_to_model_input(texts_padded)), texts_padded

