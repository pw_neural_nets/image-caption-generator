from abc import abstractmethod
import json
import os

import pandas as pd
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from PIL import Image

import config
from data_processing import transformers
from data_processing import text_processing


class CaptionDataset(Dataset):
    def __init__(self, captions, images_path, image_transform, vocab, tokenizer):
        self.images_path = images_path
        self.image_transform = image_transform
        self.vocab = vocab
        self.tokenizer = tokenizer
        self.captions = captions

    def __getitem__(self, idx):
        img = self.get_img(idx)
        text = self.get_text(idx)

        if self.image_transform:
            img = self.image_transform(img)

        return idx, img, text

    @abstractmethod
    def get_img(self, idx):
        pass

    @abstractmethod
    def get_text(self, idx):
        pass

    def process_text(self, text):
        token_seq = [token for token in self.tokenizer.process_text(text)]
        token_seq = self.add_boundary_tokens(token_seq)
        return self.vocab.numericalize(token_seq)

    def add_boundary_tokens(self, token_seq):
        return token_seq + [config.EOS]

    def get_vocab_size(self):
        return self.vocab.get_vocab_size()

    def word_idx_to_text(self, text):
        return self.vocab.textify(text)


class Flickr8kDataset(CaptionDataset):
    def __init__(self, idx2img_name, **kwargs):
        super().__init__(**kwargs)
        self.idx2img_name = idx2img_name

    def get_img(self, idx):
        img_name = self.idx2img_name.iloc[idx][0]
        img_path = os.path.join(self.images_path, img_name)
        return Image.open(img_path)

    def get_text(self, idx):
        img_name = self.idx2img_name.iloc[idx][0]
        caption_id = img_name + config.caption_idx
        text = self.captions.loc[caption_id][0]
        return self.process_text(text)

    def __len__(self):
        return len(self.idx2img_name)

    @classmethod
    def from_config(cls, img_list_path, images_path, captions, vocab=None, image_transform=None):
        idx2img_name = pd.read_csv(img_list_path, names=['image_name'], nrows=config.n_samples)

        caption_ids = idx2img_name['image_name'] + config.caption_idx
        tokenizer = text_processing.Tokenizer()
        if not vocab:
            text = captions.loc[caption_ids]['comment'].apply(lambda x: tokenizer.process_text(x))
            vocab = text_processing.Vocab.from_tokens(text, config.max_vocab, config.min_freq)

        return cls(
            idx2img_name=idx2img_name,
            captions=captions,
            images_path=images_path,
            image_transform=image_transform,
            vocab=vocab,
            tokenizer=tokenizer,
        )


class BasicCaptionDataset(CaptionDataset):
    def get_img(self, idx):
        img_name = self.captions['image_name'].iloc[idx]
        img_path = os.path.join(self.images_path, img_name)
        return Image.open(img_path).convert('RGB')

    def get_text(self, idx):
        text = self.captions['comment'].iloc[idx]
        return self.process_text(text)

    def __len__(self):
        return len(self.captions)

    @classmethod
    def from_config(cls, captions, images_path, vocab=None, tokenizer=None, image_transform=None):
        if not tokenizer and not vocab:
            tokenizer = text_processing.Tokenizer()
            text = captions['comment'].apply(lambda x: tokenizer.process_text(x))
            vocab = text_processing.Vocab.from_tokens(text, config.max_vocab, config.min_freq)
        return cls(
            captions=captions,
            images_path=images_path,
            vocab=vocab,
            tokenizer=tokenizer,
            image_transform=image_transform,
        )


DATASETS = {
    'flickr8k': Flickr8kDataset,
    'flickr30k': BasicCaptionDataset,
    'coco': BasicCaptionDataset,
}


class DataBunch:
    def __init__(self, train_ds, test_ds, valid_ds=None, collate_fn=None):
        self.train = self.to_dataloader(train_ds, collate_fn)
        self.test = self.to_dataloader(test_ds, collate_fn, shuffle=False) if test_ds else []
        self.valid = self.to_dataloader(valid_ds, collate_fn, shuffle=False) if valid_ds else []

    def __getitem__(self, mode):
        return self.__dict__[mode]

    def get_vocab_size(self):
        return self.train.dataset.get_vocab_size()

    def word_idx_to_text(self, word_idxs):
        return self.train.dataset.word_idx_to_text(word_idxs)

    def to_dataloader(self, dataset, collate_fn, shuffle=True):
        return DataLoader(
            dataset=dataset,
            batch_size=config.bs,
            shuffle=shuffle,
            collate_fn=collate_fn,
            num_workers=config.loader_workers,
        )

    @staticmethod
    def get_default_transformations():
        return transforms.Compose([
            transforms.Resize(config.image_resize),
            transforms.CenterCrop(config.image_crop),
            transforms.ToTensor(),
            transforms.Normalize(mean=config.image_normalization, std=config.image_std),
        ])

    @staticmethod
    def get_captions_coco(seed):
        captions_path = config.dataset_params['coco']['captions_path']
        with open(captions_path) as f:
            data = json.load(f)

        images = pd.DataFrame(data['images'])[['file_name', 'id']]\
            .rename(columns={"file_name": "image_name"})\
            .set_index('id')

        captions = pd.DataFrame(data['annotations'])\
            .rename(columns={"caption": "comment"})\
            .set_index('image_id')

        captions = pd.merge(captions, images, left_index=True, right_on="id")
        captions_lengths = captions['comment'].apply(lambda x: len(x.split()))
        captions = captions[captions_lengths < config.max_input_caption_length]

        test_imgs = images.sample(frac=config.test_frac, random_state=seed)
        train_captions = captions[~captions.index.isin(test_imgs.index)].reset_index(drop=True)
        test_captions = captions[captions.index.isin(test_imgs.index)].reset_index(drop=True)
        return train_captions, test_captions

    @staticmethod
    def get_captions_f30k(seed):
        captions_path = config.dataset_params['flickr30k']['captions_path']
        captions = pd.read_csv(captions_path, sep='\|\ ', nrows=config.n_samples, engine='python')
        captions = captions.dropna()
        test_imgs = pd.Series(captions['image_name'].unique()).sample(frac=config.test_frac, random_state=seed)
        train_captions = captions[~captions['image_name'].isin(test_imgs)]
        test_captions = captions[captions['image_name'].isin(test_imgs)]
        return train_captions, test_captions

    @staticmethod
    def get_captions_f8k(seed):
        captions_path = config.dataset_params['flickr8k']['train_captions_path']
        train_caption_id2text = pd.read_csv(
            captions_path, sep='\t', names=['caption_id', 'comment']).set_index('caption_id')

        captions_path = config.dataset_params['flickr8k']['test_captions_path']
        test_caption_id2text = pd.read_csv(
            captions_path, sep='\t', names=['caption_id', 'comment']).set_index('caption_id')
        return train_caption_id2text, test_caption_id2text

    @classmethod
    def get_captions_from_data(cls, seed):
        if config.dataset == 'flickr8k':
            train_captions, test_captions = cls.get_captions_f8k(seed)
        elif config.dataset == 'flickr30k':
            train_captions, test_captions = cls.get_captions_f30k(seed)
        elif config.dataset == 'coco':
            train_captions, test_captions = cls.get_captions_coco(seed)
        else:
            raise ValueError(f"dataset {config.dataset} not supported")
        return {"train": train_captions, "test": test_captions}

    @classmethod
    def from_config(cls, seed=config.SEED, collate_fn=transformers.batch_text_padding):
        image_transform = cls.get_default_transformations()
        captions = cls.get_captions_from_data(seed=seed)

        train_ds = DATASETS[config.dataset].from_config(
            image_transform=image_transform,
            captions=captions['train'],
            **config.dataset_params[config.dataset]['trainset'],
        )

        test_ds = DATASETS[config.dataset].from_config(
            image_transform=image_transform,
            captions=captions['test'],
            vocab=train_ds.vocab,
            tokenizer=train_ds.tokenizer,
            **config.dataset_params[config.dataset]['testset'],
        )

        return cls(train_ds=train_ds, test_ds=test_ds, collate_fn=collate_fn)
