from model import experiment

exp = experiment.ExperimentAttention.from_checkpoint()

exp.train_model()
print("model trained")


# exp.plot_top_results(phase='train')
# exp.plot_top_results(phase='test')
#
# exp.get_blue_scores(phase='test')
# exp.get_blue_scores(phase='train')

# phase = 'train'
#
# img, target = exp.databunch[phase].dataset[4]
# pred = (4, exp.predict(img.to(exp.device)))
# exp.plot_preds_greed([pred], phase=phase)
