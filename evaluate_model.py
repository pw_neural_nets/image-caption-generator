import torch
from model import experiment


phase = 'test'
path = './testset_predictions.pytorch'
exp = experiment.ExperimentAttention.from_checkpoint()
exp.evaluate_model(phase=phase, save_path=path)

samples = torch.load(path)
scores = exp.get_blue_scores(samples)
print(sum(scores)/len(scores))

import pdb;pdb.set_trace()
for i in range(len(samples)-4000, len(samples)-3990): exp.plot_pred_idx(samples[i]['idx'], samples[i]['y'], samples[i]['y_hat'], phase, outputname=str(i))
