### paths
# datasets 'flickr8k', 'flickr30k'
SEED = 420
dataset = 'coco'
model_logs_file = './model_logs.txt'
model_weights_path = './model_weights.pytorch'
# checkpoint_model_path = './data/coco_models/small_vocab_attn/4-8ep/model_weights.pytorch'
checkpoint_model_path = '/kaggle/input/image-caption-attn-model/model_weights.pytorch'

test_frac = 0.2
logging_interval = 128
loader_workers = 3

dataset_params = {
    'flickr8k': {
        'train_captions_path': "./data/Flickr8k/Flickr8k_text/Flickr8k.token.txt",
        'test_captions_path': "./data/Flickr8k/Flickr8k_text/Flickr8k.token.txt",
        'trainset': {
            'images_path': "./data/Flickr8k/Flickr8k_Dataset/Flicker8k_Dataset",
            'img_list_path': "./data/Flickr8k/Flickr8k_text/Flickr_8k.trainImages.txt",
        },
        'testset': {
            'images_path': "./data/Flickr8k/Flickr8k_Dataset/Flicker8k_Dataset",
            'img_list_path': "./data/Flickr8k/Flickr8k_text/Flickr_8k.testImages.txt",
        },
    },
    'flickr30k': {
        'captions_path': "/kaggle/input/flickr-image-dataset/flickr30k_images/results.csv",
        'trainset': {
            'images_path': "/kaggle/input/flickr-image-dataset/flickr30k_images/flickr30k_images",
        },
        'testset': {
            'images_path': "/kaggle/input/flickr-image-dataset/flickr30k_images/flickr30k_images",
        },
    },
    # 'flickr30k': {
    #     'captions_path': "./data/Flickr30k/results.csv",
    #     'trainset': {
    #         'images_path': "./data/Flickr30k/Flickr30k",
    #     },
    #     'testset': {
    #         'images_path': "./data/Flickr30k/Flickr30k",
    #     },
    # },
    # 'coco': {
    #     'captions_path': "./data/COCO/annotations_trainval2017/annotations/captions_train2017.json",
    #     'trainset': {
    #         'images_path': "./data/COCO/train2017",
    #     },
    #     'testset': {
    #         'images_path': "./data/COCO/train2017",
    #     },
    # },
    'coco': {
        'captions_path': "/kaggle/input/coco2017/annotations_trainval2017/annotations/captions_train2017.json",
        'trainset': {
            'images_path': "/kaggle/input/coco2017/train2017/train2017",
        },
        'testset': {
            'images_path': "/kaggle/input/coco2017/train2017/train2017",
        },
    },
}

### hyperparamters
epochs = 5
n_samples = None
lr = 0.05
bs = 256

### image
image_resize = 256
image_crop = 224
image_normalization = [0.485, 0.456, 0.406]
image_std = [0.229, 0.224, 0.225]

### text
max_vocab = 10000
min_freq = 6
max_input_caption_length = 20 # drops 0.6 % from coco with max len of 49

### CNN pretrained with hardcoded image_size = 256 -> 224 and mean/std
image_embedding_dim = 512
encoded_image_size = 7

### RNN
headstart_self_feed_step = 0
max_self_feed_step = 10 * 1835 # Coco 1835 per epoch
rnn_num_layers = 2
rnn_dropout = 0.2
word_embedding_dim = image_embedding_dim
hidden_size = 512
spacy_model = "en_core_web_sm"
beam_size = 3
max_pred_seq_len = 20

feature_maps_number = 512 # hardcoded into pretrained model
attention_dim = 512
voc_fc_dropout = 0.4

caption_idx = "#0"
BOS, EOS, UNK, PAD = 'xxbos', 'xxeos', 'xxunk', 'xxpad'
TK_MAJ, TK_UP, TK_REP, TK_WREP = 'xxmaj', 'xxup', 'xxrep', 'xxwrep'
TEXT_SPEC_TOK = [UNK, PAD, BOS, EOS, TK_MAJ, TK_UP, TK_REP, TK_WREP]
