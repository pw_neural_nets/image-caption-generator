from model.lr_finder import LearningRateFinder
from model import experiment

exp = experiment.ExperimentAttention.from_config()
lrf = LearningRateFinder(exp.model, exp.criterion, exp.optimizer)
lrf.fit(exp.databunch['train'])
lrf.plot()
