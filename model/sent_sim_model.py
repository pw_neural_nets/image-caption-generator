import torch
from torch import nn

import config
from model import image_feature_extractors

# TODO
# - bidirectional lstm


class SentenceEncoder(nn.Module):
    def __init__(self, embedding_dim, hidden_size, vocab_size, num_layers, dropout):
        super().__init__()
        self.word_embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=embedding_dim)
        self.lstm = nn.LSTM(
            input_size=embedding_dim,
            hidden_size=hidden_size,
            num_layers=num_layers,
            dropout=dropout,
        )

    def forward(self, sentence):
        embedded_words = self.word_embeddings(sentence).permute(1, 0, 2)
        out, _ = self.lstm(embedded_words)
        return out[-1]


class SentenceDecoder(nn.Module):
    def __init__(self, embedding_dim, hidden_size, vocab_size, num_layers, dropout):
        super().__init__()
        self.word_embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=embedding_dim)
        self.lstm = nn.LSTM(
            input_size=embedding_dim,
            hidden_size=hidden_size,
            num_layers=num_layers,
            dropout=dropout,
        )
        in_features = hidden_size
        self.fc = nn.Linear(in_features=in_features, out_features=vocab_size)

    def forward(self, sentence_embedding, max_len):
        word_probs = []
        out, lstm_state = self.lstm(sentence_embedding.unsqueeze(dim=0))
        new_word_probs = self.fc(out[0])
        word_probs.append(new_word_probs)
        top_w_idx = torch.argmax(new_word_probs, dim=1)

        seq_word_probs = self.iterate_self_feed_batch(
            new_top_w_idx=top_w_idx,
            lstm_state=lstm_state,
            max_len=max_len,
        )
        word_probs.extend(seq_word_probs)
        out = torch.stack(word_probs).permute(1, 0, 2)
        return out

    def iterate_self_feed_batch(self, new_top_w_idx, lstm_state, max_len):
        step = 1
        word_probs = []
        while step <= max_len:
            input_embed = self.word_embeddings(new_top_w_idx.unsqueeze(dim=0))
            out, lstm_state = self.lstm(input_embed, lstm_state)
            new_word_probs = self.fc(out[0])
            word_probs.append(new_word_probs)
            new_top_w_prob, new_top_w_idx = torch.max(new_word_probs, dim=1)
            step += 1
        return torch.stack(word_probs)


class SentenceSimilarity(nn.Module):

    def __init__(self, sentence_encoder, img_encoder, decoder):
        super().__init__()
        self.sentence_encoder = sentence_encoder
        self.img_encoder = img_encoder
        self.decoder = decoder

    def forward(self, X):
        idxs, img, txt = X
        sentence_embedding = self.sentence_encoder.forward(txt)
        img_embedding = self.img_encoder.forward(img)
        predicted_sentence = self.decoder.forward(sentence_embedding, max_len=txt.shape[1])
        return predicted_sentence, sentence_embedding, img_embedding

    @classmethod
    def from_config(cls, vocab_size):
        sentence_encoder = SentenceEncoder(
            vocab_size=vocab_size,
            embedding_dim=config.word_embedding_dim,
            hidden_size=config.hidden_size,
            num_layers=config.rnn_num_layers,
            dropout=config.rnn_dropout,
        )

        decoder = SentenceDecoder(
            vocab_size=vocab_size,
            embedding_dim=config.word_embedding_dim,
            hidden_size=config.hidden_size,
            num_layers=config.rnn_num_layers,
            dropout=config.rnn_dropout,
        )

        return cls(
            sentence_encoder=sentence_encoder,
            img_encoder=image_feature_extractors.GoogLeNet(),
            decoder=decoder,
        )
