import copy

import torch
from torch import nn

import config
from model import image_feature_extractors

#TODO
# change self feed to work from the start


class CaptionGeneratorLSTM(nn.Module):
    def __init__(self, embedding_dim, hidden_size, vocab_size, num_layers, dropout, beam_size):
        super().__init__()
        self.word_embeddings = nn.Embedding(num_embeddings=vocab_size, embedding_dim=embedding_dim)
        self.lstm = nn.LSTM(
            input_size=embedding_dim,
            hidden_size=hidden_size,
            num_layers=num_layers,
            dropout=dropout,
        )
        in_features = hidden_size
        self.fc = nn.Linear(in_features=in_features, out_features=vocab_size)
        self.softmax = nn.Softmax(dim=1)
        self.beam_size = beam_size

    def forward(self, img_embedding, sentence, self_feed_seq_part):
        assert 0 <= self_feed_seq_part <= 1, \
            f"value of self_feed_seq_part {self_feed_seq_part} has to be in range from 0 to 1"
        cutoff_step = int(sentence.shape[1] * self_feed_seq_part)

        words_probabilities = []
        max_len_seq = sentence[:, :cutoff_step].shape[1]

        input_img = img_embedding.unsqueeze(dim=0)
        out, lstm_state = self.lstm(input_img)
        single_word_probabilities = self.fc(out[0])
        words_probabilities.append(single_word_probabilities)

        if max_len_seq:
            curr_idx = self.softmax(words_probabilities[-1]).argmax(dim=-1)
            seq_word_probs = self.iterate_self_feed_batch(
                new_top_w_idx=curr_idx,
                lstm_state=lstm_state,
                max_len=max_len_seq,
            )
            words_probabilities.extend(seq_word_probs)

        for w_idx in sentence[:, cutoff_step:].T:
            input_embed = self.word_embeddings(w_idx.unsqueeze(dim=0))
            out, lstm_state = self.lstm(input_embed, lstm_state)
            single_word_probabilities = self.fc(out[0])
            words_probabilities.append(single_word_probabilities)

        return torch.stack(words_probabilities).permute(1, 0, 2)

    def iterate_self_feed_batch(self, new_top_w_idx, lstm_state, max_len):
        step = 1
        word_probs = []
        while step <= max_len:
            input_embed = self.word_embeddings(new_top_w_idx.unsqueeze(dim=0))
            out, lstm_state = self.lstm(input_embed, lstm_state)
            new_word_probs = self.fc(out[0])
            word_probs.append(new_word_probs)
            new_top_w_prob, new_top_w_idx = torch.max(new_word_probs, dim=1)
            step += 1
        return word_probs

    def sample_greedy(self, img_embedding):
        with torch.no_grad():
            idxs = []
            input_img = img_embedding.unsqueeze(dim=0)
            out, lstm_state = self.lstm(input_img)
            new_word_probs = self.fc(out[0])
            new_top_w_prob, new_top_w_idx = torch.max(new_word_probs, dim=1)
            idxs.append(new_top_w_idx)
            seq, _ = self.iterate_self_feed_single(idxs, new_top_w_idx, lstm_state)
        return seq

    def iterate_self_feed_single(self, idxs, new_top_w_idx, lstm_state, max_len=config.max_pred_seq_len):
        step = 1
        word_probs = []
        while new_top_w_idx and step <= max_len:
            input_embed = self.word_embeddings(new_top_w_idx.unsqueeze(dim=0))
            out, lstm_state = self.lstm(input_embed, lstm_state)
            new_word_probs = self.fc(out[0])
            word_probs.append(new_word_probs)
            new_top_w_prob, new_top_w_idx = torch.max(new_word_probs, dim=1)
            idxs.append(new_top_w_idx)
            step += 1
        return torch.cat(idxs), torch.stack(word_probs)

    def sample_beam(self, img_embedding):
        with torch.no_grad():
            input_img = img_embedding.unsqueeze(dim=0)
            out, lstm_state = self.lstm(input_img)
            word_probs = self.softmax(self.fc(out[0]))
            top_w_probs, top_w_idxs = torch.topk(word_probs, self.beam_size)

            hists = []
            for w_prob, w_idx in zip(top_w_probs.T, top_w_idxs.T):
                hist = {}
                hist['loss'] = -torch.log(w_prob)
                hist['idxs'] = [w_idx]
                hist['lstm_state'] = lstm_state
                hists.append(hist)

            step = 1
            while any(hist['idxs'][-1] for hist in hists) and step <= config.max_pred_seq_len:
                hists = self.beam_search(hists)
                step += 1

        return [{'idxs': torch.cat(hist['idxs']), 'loss': hist['loss']} for hist in hists]

    def beam_search(self, hists):
        all_hists = []
        for hist in hists:
            w_idx, loss, lstm_state = hist['idxs'][-1], hist['loss'], hist['lstm_state']
            if w_idx: # if not eos
                input_embed = self.word_embeddings(w_idx.unsqueeze(dim=0))
                out, lstm_state = self.lstm(input_embed, lstm_state)
                new_word_probs = self.softmax(self.fc(out[0]))
                new_top_w_probs, new_top_w_idxs = torch.topk(new_word_probs, self.beam_size)

                for new_w_prob, new_w_idx in zip(new_top_w_probs.T, new_top_w_idxs.T):
                    new_hist = copy.deepcopy(hist)
                    new_hist['loss'] = -torch.log(new_w_prob) + loss
                    new_hist['idxs'].append(new_w_idx)
                    new_hist['lstm_state'] = lstm_state
                    all_hists.append(new_hist)
            else:
                all_hists.append(hist)
        all_hists.sort(key=lambda x: min(x['loss']))
        return all_hists[:self.beam_size]


class End2EndCaptionGenerator(nn.Module):
    def __init__(self, image_feature_extractor, caption_generator):
        super().__init__()
        self.image_feature_extractor = image_feature_extractor
        self.caption_generator = caption_generator

    def forward(self, X, self_feed_seq_part=0):
        idxs, img, txt = X
        img_embedding = self.image_feature_extractor.forward(img)
        caption = self.caption_generator.forward(img_embedding, txt, self_feed_seq_part)
        return caption

    def sample_single(self, img):
        img_embedding = self.image_feature_extractor.forward(img.unsqueeze(dim=0))
        caption = self.caption_generator.sample_greedy(img_embedding)
        return caption

    def sample_beam(self, img):
        img_embedding = self.image_feature_extractor.forward(img.unsqueeze(dim=0))
        caption = self.caption_generator.sample_beam(img_embedding)
        return caption

    @classmethod
    def from_config(cls, vocab_size):
        image_feature_extractor = image_feature_extractors.GoogLeNet()
        caption_generator = CaptionGeneratorLSTM(
            vocab_size=vocab_size,
            embedding_dim=config.word_embedding_dim,
            hidden_size=config.hidden_size,
            num_layers=config.rnn_num_layers,
            dropout=config.rnn_dropout,
            beam_size=config.beam_size,
        )
        return cls(
            image_feature_extractor=image_feature_extractor,
            caption_generator=caption_generator,
        )
