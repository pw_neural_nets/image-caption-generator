import torch


class SentenceLoss(torch.nn.CrossEntropyLoss):
    def __init__(self, *args, **kwargs):
        super().__init__(reduction='none', *args, **kwargs)

    def forward(self, y_hat, targets, reduction=True):
        losses = []
        for i in range(targets.shape[1]):
            batch_losses = super().forward(y_hat[:, i], targets[:, i])
            batch_losses = torch.mean(batch_losses) if reduction else batch_losses
            losses.append(batch_losses)
        losses = torch.stack(losses)
        return torch.sum(losses, dim=0)
