from abc import abstractmethod

import torch
from torch import optim
from torch import nn
from tqdm import tqdm
import matplotlib.pyplot as plt
from nltk.translate.bleu_score import sentence_bleu
import skimage.transform

import config
from data_processing import datasets
from model import word_model
from model import sent_sim_model
from model import attention_model
from model import loss_fn

# TODO
# - add metrics tracking that works on kaggle e.g. neptune (tensorboard wont work)


class Experiment:
    def __init__(self, databunch, model, criterion, optimizer, epochs, device):
        self.databunch = databunch
        self.optimizer = optimizer
        self.criterion = criterion
        self.epochs = epochs
        self.device = device
        self.model = model
        self.logging_interval = config.logging_interval
        self._step = 0

    @abstractmethod
    def run_epoch(self, phase, epoch):
        pass

    def train_model(self):
        self.model.train()
        for epoch in range(self.epochs):
            self.run_epoch(epoch=epoch, phase='train')
            self.model.eval()
            with torch.no_grad():
                self.run_epoch(epoch=epoch, phase='test')
            self.model.train()

    def get_predictions(self, phase='test', method='greedy', k=0):
        preds = []
        coutner = 0
        for idx, img, target in enumerate(tqdm(self.databunch[phase].dataset)):
            if method == 'greedy':
                preds.append((idx, self.predict(img.to(self.device))))
            elif method == 'beam':
                preds.append((idx, self.sample_beam(img.to(self.device))[0]))
            else:
                raise ValueError()
            if k and k < coutner:
                break
            coutner += 1
        return preds

    def predict(self, img):
        y_hat = self.model.sample_single(img)
        pred = self.word_idx_to_text(y_hat)
        return pred

    def sample_beam(self, img):
        y_hat = self.model.sample_beam(img)
        y_hat = [
            {'caption': self.word_idx_to_text(y['idxs']),
             'loss': y['loss'],
             'img': img}
            for y in y_hat
        ]
        return y_hat

    def log_results(self, mode, epoch, losses, y_hat, targets, img, self_feed_seq_part):
        losses = self.log_epoch_loss(mode, epoch, losses)
        sentences = self.compare_preds_and_targets(y_hat, targets)
        sampled_sentences = self.get_sampled_sentences(img)
        self_feed_log = f"self_feed_seq_part: {self_feed_seq_part}"
        log = losses + " \n " + sentences + " \n " + sampled_sentences + "\n" + self_feed_log
        print(log)
        with open(config.model_logs_file, "a") as f:
            f.write(log)

    def log_epoch_loss(self, mode, epoch, losses):
        mean_loss = torch.mean(torch.FloatTensor(losses)) / config.bs
        return f"\nMode: {mode}. Epoch: {epoch + 1}/{config.epochs}. CrossEntropyLoss: {mean_loss}\n"

    def compare_preds_and_targets(self, y_hat, targets):
        y_hat = torch.argmax(y_hat, dim=-1)
        target_sentence = self.word_idx_to_text(targets[0])
        pred_sentence = self.word_idx_to_text(y_hat[0])
        return f"\tTarget   : '{target_sentence}'.\n \tPredicted: '{pred_sentence}'"

    def get_sampled_sentences(self, img):
        sentence = f"\tGSampling: {self.predict(img[0])}\n"
        bsample_preds = self.sample_beam(img[0])
        for i, bsample in enumerate(bsample_preds):
            sentence += f"\tBSampling: loss:{bsample['loss']}, {bsample['caption']}\n"
        return sentence

    def word_idx_to_text(self, word_idxs, remove_tokens=True):
        words = self.databunch.word_idx_to_text(word_idxs)
        if remove_tokens:
            words = words.replace(" xxpad", "").replace(" xxeos", "")
        return words

    def plot_preds_beam(self, preds, phase):
        for idx, pred in preds:
            idx, img, target = self.databunch[phase].dataset[idx]
            title = f'\n Target caption: {self.word_idx_to_text(target)}\n'
            for i in range(len(pred)):
                title += f'Pred caption: {pred[i]["caption"]} \nLoss: {pred[i]["loss"]}\n'
            plt.imshow(pred[0]['img'].permute(1, 2, 0))
            plt.title(title)
            plt.show()

    def plot_pred_idx(self, idx, target, pred, phase, bleu_score=None, outputname=None):
        idx, img, _ = self.databunch[phase].dataset[idx]
        img = img - torch.min(img)
        img = img / torch.max(img)
        fig = plt.imshow(img.permute(1, 2, 0))
        title = f'Pred caption: {pred}\n Target caption: {target}'
        if bleu_score: title += f'\nBleu score: {bleu_score}'
        plt.title(title)
        fig.axes.get_xaxis().set_visible(False)
        fig.axes.get_yaxis().set_visible(False)
        if outputname:
            plt.savefig(outputname)
        else:
            plt.show()

    def get_blue_scores(self, samples):
        targets = [s['y'].split() for s in samples]
        preds = [s['y_hat'].split() for s in samples]
        scores = []
        for i in range(len(samples)):
            scores.append(sentence_bleu([targets[i]], preds[i], weights=[1]))
        return scores

    def save_checkpoint(self):
        save_dict = {
            "model": self.model.state_dict(),
            "optimizer": self.optimizer.state_dict(),
            "vocab": self.databunch.train.dataset.vocab,
            "config": {k: v for k, v in config.__dict__.items() if not k.startswith("__")}
        }
        torch.save(save_dict, config.model_weights_path)

    @staticmethod
    def get_optimization_parameters(model):
        params_to_update = []
        for name, param in model.named_parameters():
            if param.requires_grad == True:
                params_to_update.append(param)
        return params_to_update

    @classmethod
    def from_cls_config(cls, model_cls, optimizer_cls='sgd'):
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        databunch = datasets.DataBunch.from_config()
        model = model_cls.from_config(vocab_size=databunch.get_vocab_size()).to(device)
        if optimizer_cls == "sgd":
            optimizer = optim.SGD(cls.get_optimization_parameters(model), config.lr)
        else:
            optimizer = optim.Adam(cls.get_optimization_parameters(model))
        criterion = loss_fn.SentenceLoss()
        return cls(
            databunch=databunch,
            model=model,
            criterion=criterion,
            optimizer=optimizer,
            epochs=config.epochs,
            device=device,
        )

    @classmethod
    def from_cls_checkpoint(cls, model_cls, optimizer_cls):
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        checkpoint = torch.load(config.checkpoint_model_path, map_location=device)

        databunch = datasets.DataBunch.from_config(seed=checkpoint['config']['SEED']) # itos might be in wrong

        model = model_cls.from_config(vocab_size=databunch.get_vocab_size()).to(device)
        model.load_state_dict(checkpoint['model'], strict=True)

        if optimizer_cls == "sgd":
            optimizer = optim.SGD(cls.get_optimization_parameters(model), config.lr)
        else:
            optimizer = optim.Adam(cls.get_optimization_parameters(model))
        optimizer.load_state_dict(checkpoint['optimizer'])

        criterion = loss_fn.SentenceLoss()
        return cls(
            databunch=databunch,
            model=model,
            criterion=criterion,
            optimizer=optimizer,
            epochs=config.epochs,
            device=device,
        )


class ExperimentWords(Experiment):
    def run_epoch(self, phase, epoch):
        losses = []

        for X, y in tqdm(self.databunch[phase]):
            X = [x.to(self.device) for x in X]
            y = y.to(self.device)
            self_feed_seq_part = self.calculate_self_feed_seq_part(self._step)
            y_hat = self.model.forward(X, self_feed_seq_part)

            loss = self.criterion(y_hat, y)
            losses.append(loss.detach())
            if phase == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                self._step += 1

            if losses and self._step % self.logging_interval == 0:
                self.log_results(phase, epoch, losses, y_hat.detach(), y, X[1], self_feed_seq_part)

        if phase == 'train':
            self.save_checkpoint()

    def evaluate_model(self, phase, save_path):
        results = []
        self.model.eval()
        with torch.no_grad():
            for idx, img, text in tqdm(self.databunch[phase]):

                img = img.to(self.device)
                text = text.to(self.device)
                input_text = self.text_to_model_input(text)
                y_hat = self.model.forward(img, input_text, self_feed_seq_part=1)

                loss = self.criterion(y_hat, text, reduction=False)
                y_hat = torch.argmax(y_hat, dim=-1)

                samples = [{
                    'idx': i.item(),
                    'loss': l.item(),
                    'y_hat': self.word_idx_to_text(y),
                    'y': self.word_idx_to_text(t)}
                    for i, l, y, t in zip(idx, loss, y_hat, text)
                ]
                results.extend(samples)

        results.sort(key=lambda x: x['loss'])
        torch.save(results, save_path)

    def calculate_self_feed_seq_part(self, step):
        """
        Calcualte how much of a sequence is fed from its own outputs instead of correct labels.
        Extends from the end.
        """
        if config.max_self_feed_step == 0:
            return 1

        step -= config.headstart_self_feed_step
        if step <= 0:
            self_feed_seq_part = 0
        elif step > config.max_self_feed_step:
            self_feed_seq_part = 1
        else:
            self_feed_seq_part = step / config.max_self_feed_step

        return self_feed_seq_part

    @classmethod
    def from_config(cls):
        model_cls = word_model.End2EndCaptionGenerator
        return cls.from_cls_config(model_cls, optimizer_cls='sgd')

    @classmethod
    def from_checkpoint(cls):
        model_cls = word_model.End2EndCaptionGenerator
        return cls.from_cls_checkpoint(model_cls, optimizer_cls='sgd')


class ExperimentSentenceSimilarity(Experiment):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.cos_emb_loss = nn.CosineEmbeddingLoss().to(self.device)
        self.sim_label = torch.tensor(1).to(self.device)

    def run_epoch(self, phase, epoch):
        losses = []
        for X, y in tqdm(self.databunch[phase]):
            X = [x.to(self.device) for x in X]
            y = y.to(self.device)
            predicted_text, sentence_embed, img_embed = self.model.forward(X)
            distance = self.calculate_distance(sentence_embed, img_embed)
            loss = self.criterion(predicted_text, y)
            loss += distance
            losses.append(loss.detach())

            if phase == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                self._step += 1

            if losses and self._step % self.logging_interval == 0 and self._step:
                self.log_sent_results(phase, epoch, losses, predicted_text.detach(), y)

            if phase == 'train':
                self.save_checkpoint()

    def calculate_distance(self, sentence_embed, img_embed):
        return self.cos_emb_loss(sentence_embed, img_embed, self.sim_label)

    def log_sent_results(self, mode, epoch, losses, y_hat, targets):
        losses = self.log_epoch_loss(mode, epoch, losses)
        sentences = self.compare_preds_and_targets(y_hat, targets)
        log = losses + " \n " + sentences
        print(log)
        with open(config.model_logs_file, "a") as f:
            f.write(log)

    @classmethod
    def from_config(cls):
        model_cls = sent_sim_model.SentenceSimilarity
        return cls.from_cls_config(model_cls, optimizer_cls='adam')

    @classmethod
    def from_checkpoint(cls):
        model_cls = sent_sim_model.SentenceSimilarity
        return cls.from_cls_checkpoint(model_cls, optimizer_cls='adam')


class ExperimentAttention(Experiment):
    def run_epoch(self, phase, epoch):
        losses = []
        for X, y in tqdm(self.databunch[phase]):
            X = [x.to(self.device) for x in X]
            y = y.to(self.device)
            y_hat = self.model.forward(X)
            loss = self.criterion(y_hat, y)
            losses.append(loss.detach())
            if phase == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

            if losses and self._step % self.logging_interval == 0: # and step:
                self.log_results(phase, epoch, losses, y_hat.detach(), targets=y, imgs=X[1])
            self._step += 1

        if phase == 'train':
            self.save_checkpoint()

    def evaluate_model(self, phase, save_path):
        results = []
        self.model.eval()
        with torch.no_grad():
            for X, y in tqdm(self.databunch[phase]):
                X = [x.to(self.device) for x in X]
                idx, img, _ = X
                y = y.to(self.device)
                y_hat = self.model.sample(img)
                y_hat = y_hat[0]

                samples = [{
                    'idx': i.item(),
                    'y_hat': self.word_idx_to_text(s_y),
                    'y': self.word_idx_to_text(t)}
                    for i, s_y, t in zip(idx, y_hat, y)
                ]
                results.extend(samples)

        torch.save(results, save_path)

    def log_results(self, mode, epoch, losses, y_hat, targets, imgs):
        losses = self.log_epoch_loss(mode, epoch, losses)
        sentences = self.compare_preds_and_targets(y_hat, targets)
        sampled_sentence = self.get_sampled_sentences(imgs)
        log = losses + " \n " + sentences + " \n " + sampled_sentence
        print(log)
        with open(config.model_logs_file, "a") as f:
            f.write(log)

    def get_sampled_sentences(self, imgs):
        sentence = f"\tGSampling: {self.predict(imgs[0].unsqueeze(0))}"
        return sentence

    def predict(self, img):
        y_hat, attn_hist, feature_maps = self.model.sample(img)
        pred_words = self.word_idx_to_text(y_hat[0])
        return pred_words

    def plot_attention(self, img, save_path=None):
        y_hat, attn_hist, feature_maps = self.model.sample(img)
        pred_words = self.word_idx_to_text(y_hat[0])
        feature_maps = feature_maps[0].T.detach().numpy()
        splitted_words = pred_words.split()

        for i in range(len(splitted_words)):
            feature_map_weigths = attn_hist[0, i].detach().numpy()
            heat_map = feature_map_weigths.dot(feature_maps)
            heat_map = (heat_map - heat_map.min()) / (heat_map.max() - heat_map.min())

            norm_img = (img - img.min()) / (img.max() - img.min())
            plt.imshow(norm_img[0].permute(1, 2, 0))
            plt.imshow(skimage.transform.resize(heat_map.reshape(7, 7), img.shape[2:4]), alpha=0.5, cmap='jet')
            plt.title(f"Predicted word: '{splitted_words[i]}'")
            if save_path:
                plt.savefig(save_path + f"_word_{i}")
            else:
                plt.show()

    @classmethod
    def from_config(cls):
        model_cls = attention_model.AttentionCaptionGenerator
        return cls.from_cls_config(model_cls, optimizer_cls='adam')

    @classmethod
    def from_checkpoint(cls):
        model_cls = attention_model.AttentionCaptionGenerator
        return cls.from_cls_checkpoint(model_cls, optimizer_cls='adam')
