from abc import abstractmethod

from torch import nn
import torchvision.models as models

import config

# TODO
# - add batch norm after last fc layer


class PretrainedImageFeatureExtractor(nn.Module):
    """
    Expected input size 224x224

    preprocess = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])
    """
    @abstractmethod
    def get_torch_pretrained_model(self):
        pass

    def replace_last_layer(self, model):
        model.fc = nn.Linear(
            in_features=model.fc.in_features,
            out_features=config.image_embedding_dim,
        )
        return model

    def get_pretrained_model(self):
        super().__init__()
        model = self.get_torch_pretrained_model()
        self.freeze_pretrained_layers(model)
        model = self.replace_last_layer(model)
        return model

    def freeze_pretrained_layers(self, model):
        for param in model.parameters():
            param.requires_grad = False

    def forward(self, X):
        return self.model(X)


class GoogLeNet(PretrainedImageFeatureExtractor):
    def __init__(self):
        super().__init__()
        self.model = self.get_pretrained_model()

    def get_torch_pretrained_model(self):
        model = models.googlenet(pretrained=True, progress=True)
        return model


class Resnet(PretrainedImageFeatureExtractor):
    def __init__(self):
        super().__init__()
        self.model = self.get_pretrained_model()

    def get_torch_pretrained_model(self):
        model = models.resnet101(pretrained=True, progress=True)
        return model

